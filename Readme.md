#Server actions
##Creating a new server
To start with you will need a base server image with the correct ssh keys installed

  - Download an Ubuntu Server 64-bit 14.04 LTS image from http://www.ubuntu.com/download/server
  - Install that into Virtual Box using the following settings:-
    - name: Called It
    - type: Ubuntu 64-bit
    - memory: 1024MB
    - disk: 50GB
    - user:
      - full name: Ubuntu
      - login: ubuntu
      - password: ubuntu (you will get a warning about a weak password - ignore it)
      - packages to install:
        - openssh
  - After the install is complete and the server reboots, shut it down
  - Configure the virtual machine for network access by running this on your host machine:-

```.text
VBoxManage modifyvm 'Called It' --natpf1 ssh,tcp,,2222,,22
VBoxManage modifyvm 'Called It' --natpf1 http,tcp,,8080,,80
```

  - Start the Called It virtual machine
  - Configure your host machine to make accessing the Called It virtual machine easier
    - add ```127.0.0.1   calledit``` to your /etc/hosts file
    - add the following to your .ssh/config file (create it if it does not exist)

```.text
Host calledit
Port 2222
User ubuntu
IdentityFile ~/.ssh/calledit
```
  
  - If you have the calledit ssh keypair, copy the public key into the virtual machine. If you don't, then create a keypair (```ssh-keygen -f calledit```). Copy the private key to your .ssh folder and the public key to the virtual machine:-

    - copying the private key to your .ssh folder:-

```.text
cp calledit ~/.ssh
chmod 600 ~/.ssh/calledit
```
    - copying the public key to the virtual machine:-

```.text
scp calledit.pub calledit:
ssh calledit
mkdir .ssh
chmod 700 .ssh
mv calledit.pub .ssh/authorized_keys
chmod 600 .ssh/authorized_keys
exit
```

  - Update the server (this step is optional, but recommended)

```.text
ssh calledit
sudo apt-get update && apt-get -y dist-upgrade && reboot
```


##Provisioning the Called It virtual machine
Once you have a working base virtual machine you can provision it for Called It using Ansible. You can also use this command to reconfigure the Called It service or to refresh the code on the virtual machine.

  - Install Ansible

  ```sudo pip install ansible```

  - Provision Called It

  ```ansible-playbook -i inventory/development calledit.yml -K --ask-vault-pass```

The sudo password is ```ubuntu```. The vault password will be provided through a back channel

##(Re)Setting the initial database
You can initialise the database at anytime using the seed-db playbook:-

```ansible-playbook -i inventory/development seed-db.yml -K```

See the assets/database/seed_data.sql file in the api project for the initial seed data. Feel free to add anything you think will be useful.


