#! /usr/bin/env sh

VM=CalledIt

# get the VM's settings
eval $(VBoxManage showvminfo --machinereadable "$VM" | egrep '(VMState|nic1)')

# shutdown VM before configuring
if [ $VMState = 'running' ]
then
	VBoxManage controlvm "$VM" poweroff
fi

# ensure that nic1 is set to NAT
if [ $nic1 != 'nat' ]
then
	VBoxManage modifyvm "$VM" --nic1 nat
fi
VBoxManage modifyvm "$VM" --natpf1 "guestssh,tcp,,2222,,22"

# start VM
VBoxManage startvm "$VM"
